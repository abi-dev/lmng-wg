import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { check } from "meteor/check";

export const Payments = new Mongo.Collection("payments");

if (Meteor.isServer) {
    // This code only runs on the server
    Accounts.onCreateUser((options, user) => {
        if (options.isHuman) user.isHuman = options.isHuman;
        else user.isHuman = false;

        if (options.profile) {
            user.profile = options.profile;
        }

        return user;
    });

    Meteor.publish("Meteor.users.isHuman", function() {
        const options = {
            fields: {
                isHuman: 1
            }
        };

        return Meteor.users.find({}, options);
    });

    Meteor.publish("userData", function() {
        if (this.userId) {
            return Meteor.users.find(
                {},
                {
                    fields: { isHuman: 1, username: 1, roles: 1 }
                }
            );
        } else {
            this.ready();
        }
    });

    Meteor.publish("paymentsData", function() {
        if (this.userId) {
            return Payments.find(
                {},
                {
                    fields: { _id: 1, status: 1, user: 1, amount: 1, debtors: 1, createdAt: 1, username: 1 }
                }
            );
        } else {
            this.ready();
        }
    });
}

Meteor.methods({
    insertUser(username, password, role) {
        check(username, String);
        check(password, String);

        let newId = Accounts.createUser({
            username: username,
            password: password,
            isHuman: true
        });

        if (newId !== undefined) {
            if (role) Roles.addUsersToRoles(newId, role, Roles.GLOBAL_GROUP);
            return "success";
        } else {
            return "crt-err";
        }
    },
    deleteUser(id) {
        if (Roles.userIsInRole(this.userId, "admin", Roles.GLOBAL_GROUP)) {
            Meteor.users.remove({ _id: id });
            return "success";
        } else {
            return "not-authorized";
        }
    },
    insertPayment(user, amount, debtors) {
        check(user, String);
        check(debtors, Array);

        if (!this.userId) {
            throw new Meteor.Error("not-authorized");
        }

        Payments.insert({
            status: true,
            latestStatusChange: null,
            user: user,
            amount: amount,
            debtors: debtors,
            createdAt: new Date(),
            username: Meteor.users.findOne(user).username
        });
    },
    setPaymentStatus(paymentId, newStatus) {
        check(paymentId, String);
        check(newStatus, Boolean);

        const payment = Payments.findOne(paymentId);

        Payments.update(paymentId, { $set: { status: newStatus, latestStatusChange: new Date() } });
    },
    updateUserPayedAmount(user, paymentId, newAmount) {
        check(user, String);
        check(paymentId, String);
        check(newAmount, Number);

        const payment = Payments.findOne(paymentId);
        if (!payment) throw new Meteor.Error("payment-not-found");
        if (!payment.status == false) throw new Meteor.Error("payment-inactive");

        if (payment.debtors.hasOwnProperty(user)) {
            let newDebtors = payment.debtors;
            newDebtors[user].payed_amount = newAmount;

            Payments.update(paymentId, { $set: { debtors: newDebtors } });
        } else {
            throw new Meteor.Error("user-not-debtor");
        }
    }
});
