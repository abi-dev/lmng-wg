import { Accounts } from "meteor/accounts-base";
import { Meteor } from "meteor/meteor";

if (Meteor.isClient) {
    Accounts.ui.config({ passwordSignupFields: "USERNAME_ONLY" });
}
