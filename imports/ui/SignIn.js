import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Meteor } from "meteor/meteor";

import "./SignIn.css";

export default class SignIn extends Component {
    onLoginButtonClick() {
        let username = ReactDOM.findDOMNode(this.refs.inputUsername).value,
            password = ReactDOM.findDOMNode(this.refs.inputPassword).value;

        Meteor.loginWithPassword(
            {
                username: username
            },
            password,
            err => {
                if (err) {
                    console.log(err);
                }
            }
        );
    }

    render() {
        return (
            <div className="sign-in-parent">
                <div className="wrapper">
                    <div className="sign-in-child">
                        <img id="sign-in-logo" src="/lmng_logo_circle.png" alt="lmng logo" />
                    </div>
                    <div className="sign-in-child">
                        <div className="sign-in-child-row">
                            <input
                                ref="inputUsername"
                                className="login-input"
                                type="text"
                                name="name"
                                placeholder="username"
                                size="10"
                            />
                            <input
                                ref="inputPassword"
                                className="login-input"
                                type="password"
                                name="password"
                                placeholder="password"
                                size="10"
                            />
                        </div>
                        <div className="sign-in-child-row">
                            <button className="login-button" onClick={this.onLoginButtonClick.bind(this)}>
                                Login
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
