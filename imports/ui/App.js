import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import { Button } from "reactstrap";

import Dashboard from "./Dashboard.js";
import SignIn from "./SignIn.js";

import "./App.css";

class App extends Component {
    render() {
        return <div className="container">{this.props.currentUser ? <Dashboard /> : <SignIn />}</div>;
    }
}

export default withTracker(() => {
    return {
        currentUser: Meteor.user()
    };
})(App);
