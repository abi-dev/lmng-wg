import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import "./Dashboard.css";

import Time from "./dashboard/Time";
import Overview from "./dashboard/Overview";
import Manage from "./dashboard/Manage";
import AddUser from "./dashboard/AddUser";
import Overlay from "./Overlay";

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            curPage: "Overview", // default to the overview; 'Overview', 'Manage', 'AddUser',
            curOverlay: "none" // default to none; 'none', 'AddPayment'
        };
    }

    onLogoutClick() {
        Meteor.logout();
    }

    onManageButtonClick() {
        this.setState({
            curPage: "Manage"
        });
    }

    changeCurPage(newPage) {
        this.setState({
            curPage: newPage
        });
    }

    changeCurOverlay(newOverlay) {
        this.setState({
            curOverlay: newOverlay
        });
    }

    render() {
        return (
            <div className="dashboard-parent">
                <nav className="navbar navbar-expand-lg navbar-wg">
                    <a className="navbar-brand" href="">
                        <img id="navbar-logo" src="/lmng_logo_lowres.png" alt="lmng logo" />
                    </a>

                    <div className="navbar-custom-div">
                        <Time />
                    </div>
                    <div className="navbar-account">
                        <span className="nav-span">{Meteor.user().username}</span>
                        <span className="nav-span">|</span>
                        {Roles.userIsInRole(Meteor.user()._id, ["admin"]) && (
                            <>
                                <a className="nav-link" onClick={this.onManageButtonClick.bind(this)}>
                                    Manage
                                </a>
                                <span className="nav-span">|</span>
                            </>
                        )}
                        <a className="nav-link" onClick={this.onLogoutClick.bind(this)} href="">
                            Logout
                        </a>
                    </div>
                </nav>
                <div className="content">
                    {this.state.curPage === "Overview" && (
                        <Overview
                            changeCurPage={this.changeCurPage.bind(this)}
                            changeCurOverlay={this.changeCurOverlay.bind(this)}
                        />
                    )}
                    {this.state.curPage === "Manage" && <Manage changeCurPage={this.changeCurPage.bind(this)} />}
                    {this.state.curPage === "AddUser" && <AddUser changeCurPage={this.changeCurPage.bind(this)} />}
                </div>
                <Overlay
                    changeCurOverlay={this.changeCurOverlay.bind(this)}
                    changeCurPage={this.changeCurPage.bind(this)}
                    curOverlay={this.state.curOverlay}
                />
            </div>
        );
    }
}

export default withTracker(() => {
    Meteor.subscribe("Meteor.users.isHuman");

    return {
        currentUser: Meteor.user()
    };
})(Dashboard);
