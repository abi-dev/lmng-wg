import React, { Component } from "react";

import "./Overlay.css";
import AddPayment from "./overlay/AddPayment";

export default class Overlay extends Component {
    constructor(props) {
        super(props);
    }

    onExitClick() {
        this.props.changeCurOverlay("none");
    }

    render() {
        return (
            <>
                {this.props.curOverlay === "none" && <></>}
                {this.props.curOverlay !== "none" && (
                    <div className="overlay-parent">
                        <div className="overlay-flex-parent">
                            <div className="overlay-content-wrapper">
                                <div className="overlay-row">
                                    <a className="material-icons exit-icon" onClick={this.onExitClick.bind(this)}>
                                        clear
                                    </a>
                                </div>
                                <hr />
                                {this.props.curOverlay === "AddPayment" && (
                                    <AddPayment
                                        changeCurPage={this.props.changeCurPage}
                                        changeCurOverlay={this.props.changeCurOverlay}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                )}
            </>
        );
    }
}
