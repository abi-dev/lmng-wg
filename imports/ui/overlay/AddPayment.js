import React, { Component } from "react";
import { Meteor } from "meteor/meteor";
import ReactDOM from "react-dom";
import { withTracker } from "meteor/react-meteor-data";

import "./AddPayment.css";

class AddPayment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            debtors: []
        };
    }

    onDoneClick() {
        let userId = ReactDOM.findDOMNode(this.refs.paymentUserId).value,
            amount = ReactDOM.findDOMNode(this.refs.paymentAmount).value;

        Meteor.call("insertPayment", userId, amount, this.state.debtors, (err, ret) => {});
        this.props.changeCurOverlay("none");
    }

    onAddDebtorClick() {
        let newDebtor = ReactDOM.findDOMNode(this.refs.paymentNewDebtor).value;
        let debtors = this.state.debtors;

        if (
            debtors
                .map(e => {
                    return e.userId;
                })
                .indexOf(newDebtor) != -1
        ) {
            /* console.log("already a debtor"); */
            return;
        }

        debtors.push({
            userId: newDebtor,
            username: Meteor.users.find({ _id: newDebtor }).fetch()[0].username,
            payedAmount: 0,
            deadline: null,
            toPayPerc: null
        });
        this.setState({ debtors: debtors });
    }

    onDeleteDebtorClick(dId) {
        let debtors = this.state.debtors;
        debtors.map((d, index) => {
            if (d.userId == dId) debtors.splice(index, 1);
        });

        this.setState({ debtors: debtors });
    }

    getDebtors() {
        let displayDebtors = this.state.debtors.map(debtor => {
            return (
                <tr key={debtor.userId + "debtorsTable"}>
                    <th scope="row">{debtor.userId}</th>
                    <td>{debtor.username}</td>
                    <td>{debtor.payedAmount}</td>
                    <td>{debtor.toPayPerc}</td>
                    <td className="controls">
                        <a
                            className="material-icons custom-icons"
                            onClick={this.onDeleteDebtorClick.bind(this, debtor.userId)}
                        >
                            delete_circle
                        </a>
                    </td>
                </tr>
            );
        });

        return displayDebtors;
    }

    render() {
        return (
            <div className="addpayment-parent">
                <div className="addpayment-row">
                    <div>
                        <h1>Add Payment</h1>
                    </div>
                    <div>
                        <select ref="paymentUserId">
                            {this.props.users.map(user => {
                                return (
                                    <>
                                        {Roles.userIsInRole(user._id, ["member"]) && (
                                            <option key={user._id + "user"} value={user._id}>
                                                {user.username}
                                            </option>
                                        )}
                                    </>
                                );
                            })}
                        </select>
                        <input
                            ref="paymentAmount"
                            className="payment-input"
                            type="number"
                            name="amount"
                            min="1"
                            placeholder="€"
                            size="5"
                        />
                        <a className="material-icons custom-icons payment-icons" onClick={this.onDoneClick.bind(this)}>
                            done
                        </a>
                    </div>
                </div>
                <div className="addpayment-row">
                    <div>
                        <h3>Debtors</h3>
                    </div>
                    <div>
                        <label> Add Debtor </label>
                        <select ref="paymentNewDebtor">
                            {this.props.users.map(user => {
                                return (
                                    <>
                                        {Roles.userIsInRole(user._id, ["member"]) && (
                                            <option key={user._id + "debtor"} value={user._id}>
                                                {user.username}
                                            </option>
                                        )}
                                    </>
                                );
                            })}
                        </select>
                        <a
                            className="material-icons custom-icons payment-icons"
                            onClick={this.onAddDebtorClick.bind(this)}
                        >
                            add
                        </a>
                    </div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Payed Amount</th>
                                <th scope="col">To Pay Percentage</th>
                                <th scope="col">Controls</th>
                            </tr>
                        </thead>
                        <tbody>{this.getDebtors()}</tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default withTracker(() => {
    Meteor.subscribe("userData");

    return {
        users: Meteor.users
            .find(
                {},
                {
                    sort: {
                        createdAt: -1
                    }
                }
            )
            .fetch()
    };
})(AddPayment);
