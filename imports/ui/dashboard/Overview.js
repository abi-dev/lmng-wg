import React, { Component } from "react";

import "./PaymentsOverview";

import "./Overview.css";
import PaymentsOverview from "./PaymentsOverview";

export default class Overview extends Component {
    render() {
        return (
            <div className="overview-parent">
                <div className="overview-row" />
                <div className="overview-row">
                    {" "}
                    <PaymentsOverview
                        changeCurPage={this.props.changeCurPage}
                        changeCurOverlay={this.props.changeCurOverlay}
                    />{" "}
                </div>
            </div>
        );
    }
}
