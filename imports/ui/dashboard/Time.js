import React, {Component} from 'react';

import './Time.css';

export default class Time extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({date: new Date()});
    }

    render() {
        return (
            <div className="time-text">
                {this
                    .state
                    .date
                    .toLocaleTimeString()}
            </div>
        );
    }
}
