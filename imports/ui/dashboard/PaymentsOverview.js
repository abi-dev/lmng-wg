import React, { Component } from "react";
import { withTracker } from "meteor/react-meteor-data";

import "./PaymentsOverview.css";
import { Payments } from "../../api/tasks";

class PaymentsOverview extends Component {
    constructor(props) {
        super(props);
    }

    getPayments() {
        let displayPayments = this.props.payments.map(payment => {
            return (
                <tr key={payment.userId}>
                    <th scope="row">{payment.username}</th>
                    <td>{payment.status.toString()}</td>
                    <td>{payment.amount}</td>
                    <td>
                        {payment.debtors.map(user => {
                            return (
                                <span key={user.userId} className="badge badge-info">
                                    {user.username}
                                </span>
                            );
                        })}
                    </td>
                    <td>{payment.createdAt.toDateString()}</td>
                </tr>
            );
        });

        return displayPayments;
    }

    onAddPaymentClick() {
        this.props.changeCurOverlay("AddPayment");
    }

    render() {
        return (
            <div className="payments-overview-parent">
                <h1>
                    {" "}
                    Payments{" "}
                    <a
                        className="material-icons custom-icons payment-icons"
                        onClick={this.onAddPaymentClick.bind(this)}
                    >
                        add
                    </a>{" "}
                </h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Status</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Debtors</th>
                            <th scope="col">Created At</th>
                        </tr>
                    </thead>
                    <tbody>{this.getPayments()}</tbody>
                </table>
            </div>
        );
    }
}

export default withTracker(() => {
    Meteor.subscribe("paymentsData");

    return {
        payments: Payments.find(
            {},
            {
                sort: {
                    createdAt: -1
                }
            }
        ).fetch()
    };
})(PaymentsOverview);
