import React, { Component } from "react";
import { Meteor } from "meteor/meteor";
import ReactDOM from "react-dom";

import "./AddUser.css";

export default class AddUser extends Component {
    constructor(props) {
        super(props);
    }

    onDoneClick() {
        let username = ReactDOM.findDOMNode(this.refs.signupUsername).value,
            password = ReactDOM.findDOMNode(this.refs.signupPassword).value,
            password2 = ReactDOM.findDOMNode(this.refs.signupPassword2).value,
            role = ReactDOM.findDOMNode(this.refs.signupRole).value;

        if (password == password2) {
            Meteor.call("insertUser", username, password, role, (err, ret) => {});
            this.props.changeCurPage("Manage");
        }
    }

    render() {
        return (
            <div className="adduser-parent">
                <div className="adduser-inputs">
                    <div>
                        <h1>Signup</h1>
                    </div>
                    <div>
                        <input
                            ref="signupUsername"
                            className="signup-input"
                            type="text"
                            name="name"
                            placeholder="username"
                            size="10"
                        />
                        <input
                            ref="signupPassword"
                            className="signup-input"
                            type="password"
                            name="password"
                            placeholder="password"
                            size="10"
                        />
                        <input
                            ref="signupPassword2"
                            className="signup-input"
                            type="password"
                            name="password"
                            placeholder="password rep."
                            size="10"
                        />
                        <select ref="signupRole">
                            <option value="admin">admin</option>
                            <option value="member">member</option>
                        </select>
                        <a className="material-icons custom-icons signup-icons" onClick={this.onDoneClick.bind(this)}>
                            done
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
