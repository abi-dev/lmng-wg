import React, { Component } from "react";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import "./Manage.css";

class Manage extends Component {
    constructor(props) {
        super(props);
    }

    getUsers() {
        let users = this.props.users.map(user => {
            let roles = [];

            if (user.roles !== undefined)
                roles = user.roles.__global_roles__.map(role => {
                    if (role == "admin")
                        return (
                            <span key={role} className="badge badge-danger">
                                {role}
                            </span>
                        );
                    else
                        return (
                            <span key={role} className="badge badge-secondary">
                                {role}
                            </span>
                        );
                });

            if (!user.isHuman)
                roles.push(
                    <span key={user._id + "bot"} className="badge badge-info">
                        bot
                    </span>
                );

            return (
                <tr key={user._id}>
                    <th scope="row">{user._id}</th>
                    <td>{user.username}</td>
                    <td>{roles}</td>
                    <td className="controls">
                        <a
                            className="material-icons custom-icons"
                            onClick={this.onDeleteUserClick.bind(this, user._id)}
                        >
                            delete_circle
                        </a>
                    </td>
                </tr>
            );
        });
        return users;
    }

    onDeleteUserClick(id) {
        Meteor.call("deleteUser", id, (err, ret) => {});
    }

    onAddUserClick() {
        this.props.changeCurPage("AddUser");
    }

    render() {
        return (
            <div className="manage-parent">
                <h1>
                    Users
                    <a className="material-icons h1-icons custom-icons" onClick={this.onAddUserClick.bind(this)}>
                        add_circle
                    </a>
                </h1>
                <div className="manage-users-parent">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Roles</th>
                                <th scope="col">Controls</th>
                            </tr>
                        </thead>
                        <tbody>{this.getUsers()}</tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default withTracker(() => {
    Meteor.subscribe("userData");

    return {
        users: Meteor.users
            .find(
                {},
                {
                    sort: {
                        createdAt: -1
                    }
                }
            )
            .fetch()
    };
})(Manage);
