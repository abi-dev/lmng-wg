import { Meteor } from "meteor/meteor";
import "../imports/api/tasks.js";

import "../imports/startup/accounts-config.js";

Meteor.startup(() => {
    Meteor.users.remove({ username: "adminuser" });

    let newId = Accounts.createUser({
        username: "adminuser",
        email: "admin@lmngs.gg",
        password: "951",
        isHuman: false
    });
    Roles.addUsersToRoles(newId, "admin", Roles.GLOBAL_GROUP);
});
